#!/usr/bin/bash
#!/usr/bin/sh
# Point to projet repo 
cd ~/public_html/dev/frontend

# Update project
git fetch --all && git reset --hard && git pull origin develop

# Install depondency
/opt/alt/alt-nodejs12/root/usr/bin/npm i 
/opt/alt/alt-nodejs12/root/usr/bin/npm run build:dev