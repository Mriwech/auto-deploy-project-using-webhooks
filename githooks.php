<?php
// Spécifiez le chemin absolu vers le fichier shell
$scriptPathBack     = '~/public_html/auto-deploy/auto_update_dev_back.sh';
$scriptPathFront    = '~/public_html/auto-deploy/auto_update_dev_front.sh';
$scriptPathNpm      = '~/public_html/auto-deploy/npm_install.sh';
$deployBackLog      = "./log/log_deploy_back.log";
$deployFrontLog     = "./log/log_deploy_front.log";
$errorLog           = "./log/dev_error.log";

echo $_SERVER['HTTP_X_GITLAB_TOKEN'] ?? "Pas de token";

function execInBackground($cmd) {
    exec($cmd . " > /dev/null &");
}

// Vérifiez si la demande provient d'un webhook GitLab
if ($_SERVER['HTTP_X_GITLAB_TOKEN'] === 'merge_in_develop_back') {
    // Exécutez le script shell
    execInBackground("sh $scriptPathBack");
    
    // Chemin du fichier log où les erreurs doivent être sauvegardées
    error_log($output, 3, $deployBackLog);
} else if ($_SERVER['HTTP_X_GITLAB_TOKEN'] === 'merge_in_develop_front') {
    // Exécutez le script shell
    execInBackground($scriptPathFront);

    // Chemin du fichier log où les erreurs doivent être sauvegardées
    error_log($output, 3, $deployFrontLog);
} else {
    // Si la demande ne provient pas d'un webhook GitLab, affichez un message d'erreur
    $output = 'Erreur : Cette page est accessible uniquement via un webhook GitLab.';
    // Enregistrement du message d'erreur dans le fichier log
    error_log($output, 3, $errorLog);
    
}