#!/usr/bin/env php
#!/usr/bin/bash
#!/opt/cpanel/composer/bin/composer

# Point to projet repo 
cd ~/public_html/dev/backend-php

# Fetch and Reset changes
echo `git fetch --all && git reset --hard && git pull origin develop`

# Install new dependances
export COMPOSER_HOME="$HOME/.config/composer";
/opt/cpanel/composer/bin/composer install --ignore-platform-reqs
/opt/cpanel/composer/bin/composer dump-env dev

# Remove ApiPlatform Cache et Run make:install
rm -rf ./var
php bin/console cache:clear

# Regenerate migration and Data Fixtures
php bin/console doctrine:database:drop --force --if-exists --no-interaction
php bin/console doctrine:database:create --if-not-exists --no-interaction
php bin/console doctrine:migrations:migrate --no-interaction
php bin/console doctrine:fixtures:load --no-interaction
php bin/console lexik:jwt:generate-keypair --overwrite --no-interaction